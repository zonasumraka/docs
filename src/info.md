![Build Status](https://gitlab.com/bamhm182/mdbook/badges/master/pipeline.svg)

---

Example [mdBook][] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
# requiring the environment of Rust 1.42.0
image: rust:1.42.0

before_script:
  - wget -qO- https://github.com/rust-lang/mdBook/releases/download/v0.3.7/mdbook-v0.3.7-x86_64-unknown-linux-gnu.tar.gz | tar -xvz -C /usr/local/bin
  - chmod 0755 /usr/local/bin/mdbook
    
# the 'pages' job will build your site and deploy if on master
pages:
  stage: deploy
  script:
    - mdbook build $([ "$CI_COMMIT_BRANCH" = "master" ] && echo "-d public") # build to public path on master
  artifacts:
    paths:
      - public
      - book
    expire_in: 1 day
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install mdBook][install]
1. Preview your project: `mdbook serve`
1. Add content
1. Generate the website: `mdbook build` (optional)
1. Push your changes to the master branch: `git push`

Read more at mdbook's [documentation][].

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

----

Forked from @bamhm182

[ci]: https://about.gitlab.com/gitlab-ci/
[mdBook]: https://rust-lang.github.io/mdBook/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: https://rust-lang.github.io/mdBook/cli/index.html
[documentation]: https://rust-lang.github.io/mdBook/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

